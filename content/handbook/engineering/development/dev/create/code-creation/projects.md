---
title: "Create:Code Creation Project List"
description: A list of current and past projects for the Create:Code Creation team
---

### Active Projects

| Start Date | Project | Description | Tech Lead |
|------------|---------|-------------|-----------|
| 2023-11-01 | [Repository X-Ray](https://gitlab.com/groups/gitlab-org/-/epics/11733) | Gather information about a GitLab repository to provide extra context when generating code suggestions |           |
|            |         |             |           |

### Archived Projects 

| Start Date | End Date | Project | Description | Tech Lead |
|------------|----------|---------|-------------|-----------|
|            |          |         |             |           |
|            |          |         |             |           |
